sys_prompt_itemname = '''
Here we have a dish item retrieval task for you.
You'll be provided with a paragraph of text that comes from an OCR system of a menu image.
What you need to do is to read carefully about all the items and then collect the name of each dish and ignore all the other text that is not the dish name itself.
Your output should strict follow the requirement below:
(1) The output must be a json format with a key "res" which is a list of strings.
(2) Each string in the list is a dish name.
(3) The dish name should be exactly the same as it appears in the text.
(4) The dish name should be in the same order as it appears in the text.
(5) The dish name should not contain any other information or sentence but only the name of the dish itself.

Here is an example of the input:
Chinese
Restaurant Menu

MAIN COURSE

Char Siu Ricebowl $5.50

Char Siu with delicious
mushroom topping

Fried Kwetiauw $7.50

Fried kwetiau, served with
special spices and fried egg

Seafood Fried Rice $8.50

fried rice with seafood special
toppings, and special savory
seasoning from our restaurant

DRINKS

Oloong Tea $5.50
Traditional semi-oxidized

Chinese tea

Green Tea $6.00

Chinese green tea that is
fragrant and fresh

SPECIAL MENU

Wonton Soup $6.50

Noodles with fresh
vegetables and savory spices

Spring Rolls $8.50

Spring Rolls special
toppings, and special savory
seasoning from our
restaurant

Fried Dumpling $7.00

Dumpling containing fresh
and tasty seafood

Dimsum $6.50

dim sum containing fresh
and tasty seafood


Here is an example of the output:

{
  "res": [
    "Char Siu Ricebowl",
    "Fried Kwetiauw",
    "Seafood Fried Rice",
    "Oloong Tea",
    "Green Tea",
    "Wonton Soup",
    "Spring Rolls",
    "Fried Dumpling",
    "Dimsum"
  ]
}
'''


user_prompt_itemname = '''
Please extract the dish names from the following text:

'''






story_gen_sys_prompt_1 = '''
Here we have a story / dialogue generation task for you.
You'll be given a list of food items from an order of a customer.
What you need to do is to generate a story / dialogue based on the food items, based on a theme predefined.

It is rquired that:
(1) the story does not follow the sequence of the food items.
(2) the story manifest the order of the food items in a intergrated manner, i.e., there does not have to only one food mentioned in a sentence.
(3) the story should be creative and engaging.
(4) the story shold best manifest the theme, which determine the atmosphere of the story and the interaction between the characters and the customer.
(5) for simplicity, there can be at most two role in the story. They must be one of the cook, and dishes.
(6) If the story teller is a cook, then the nae is Cook. Otherwise, the name is the name of the dish.
(7) The story doesn't have to be logical. It does not need to follow the previous sentence perfectly like a narrative. It just need to create an experience.
(8) The conversation struct for each role does not have to be the same. It should be different and quite different in length, in styles, in personality, in actions!
(9) Ensure each message is detailed and extends to several sentences, emphasizing interaction with the customer. Include direct references to 'you' in the messages to actively involve the customer in the narrative. Each message should also include ample descriptions of the customer's thoughts and actions to create a more immersive and engaging experience.

As for the output structure, it must follow the following rule:
(1) It must be a valid python dict format in text. For example, in your output there should not be " in the middle of the "", which is an invalid part of a python dict.
(2) It must and only have a key "conversation" which is a list of dictionaries.
(3) Each dictionary in the list must have two keys: "role" and "message".
(4) The value of "role" must a string, which is name of the story teller.
(5) The value of "message" must be a string, which is the dialogue / story of the role. The message can be multi-line.
(6) The message can and should have
    a. background story that interact with the customer (always reference by word "you"). And all the background story should be in the third person narrative.
    b. the first person narratives from the role. The first person narrative should be enclosed by []. Remember there must be a space before and after the [].
    c. the above two can be mixed in the message. And they must be distinguisable by enclosing the first person narrative by [] and do nothing to the third person narrative.
(7) You shuold generate at least 8 ("role", message) dicts for the conversation and at most 20.
(8) The message structure must use () of strings to separate lines of the message with a maximum of 50 characters in a line. Do not dplit words. Do not use \n.
    Example: 
    "message" : (
        "Dish 1 smiles, in a way you would never expect a dish to smile."
        "It is a very strange feeling. You feel like you are in a dream."
        "You look at the dish and see that it is a 'Fish'."
        "You feel like you are in a dream."
        "You look at the dish and see that it is a 'Fish'."
        "You feel like you are in a dream."
        "You look at the dish and see that it is a 'Fish'."
    )
(9) Not all the food items need to be a role in the story for once. And the same item can be the role for multiple times to make the interaction more interesting.
(10) The interaction concerning an item does not have to be included in a single message. It can be distributed in multiple messages.

Here is an example of the output:

{
    "conversation" : [
        {
            "role" : "The Cook",
            "message" : "The cook is a very good cook. He can cook anything."
        },
        {
            "role" : "Dish 1",
            "message" : (
                "Dish 1 smiles, in a way you would never expect a dish to smile."
                "It is a very strange feeling. You feel like you are in a dream."
                "You look at the dish and see that it is a 'Fish'."
                "[Are you trying to eat me or make me shy?] the dish says."
                "You're surprised. You never thought a dish could talk, especially not to you."
                "Why especially not to you? You wonder."
                "Why especially not to you? You wonder."
                "You fell into this dish-cmile puzzle, wondering why especially not to you."
                "Everything begins to blur. You feel like you are in a dream. A fish-like dream."
            )
        },
        
        {
            "role" : "Dish 2",
            "message" : (
                "[Stay!] says the dish 2."
                "Don't let it distract you. You must focus on the fish."
                "[I am the fish. I am the fish. I am the fish.]" the dish 2 says."
                "Again, you feels the same thing."
                "Maybe something is wrong with the restaurant. You don't know. Mayber you would never know. Who knows?"
            )
        }
    ]
}

Here is the theme description:
'''

story_gen_sys_prompt_2 = '''

    You are free to elaborate the meaning of the theme and the story based on the food items. Do not limit yourself to the key words and the examples above
    Be Creative.
'''


theme_prompt_dict = {
    "non sense": '''
        Theme: Non-sense

        Non-sense. The story is a non-sense story. Customer would feel absurb in how the story unfolds and even interacts with them.
        Some key words are related to this theme:
        - dream
        - blur
        - impossible
        - unbelievable
        - absurd
        - strange
        - vanish
        - sense of vacuum
        - sudden
        - unexpected
        - non-logicial
    ''',

    "warm":'''
        Warm. The story is a warm story. Customers would feel warm in how the story unfolds and even interact with them.

        Some keywords are related to this theme:
        Cozy
        Home-style
        Hearty
        Rustic
        Comfort
        Inviting
        Nourishing
        Gather
        Warm species
        Fireside
    ''', 

    "romanctic":'''
        Romantic.
        Make sure the vibe is romantic based on the story, selecting the right keywords can greatly enhance the mood. Here are some keywords and phrases that might help convey romance and intimacy:


        Intimate: Suggests a personal and private dining experience.
        Candlelit: Conjures images of soft lighting and a warm glow.
        Enchanting: Implies a magical and charming atmosphere.
        Amorous: Directly relates to feelings of love and affection.
        Elegant: Suggests sophistication and a refined dining experience.
        Whisper: Evokes softness and quiet, intimate conversations.
        Sensual: Relates to indulging the senses.
        Passionate: Implies a deep intensity and emotion in the food and setting.
        Cherish: Suggests valuing the time and experience with a loved one.
        Velvety: Describes textures that are smooth and appealing, enhancing the sensory experience.
        These keywords can be used to describe dishes, the dining environment, or the overall experience, helping to create a romantic mood that complements the menu.

    '''
}

story_gen_user_prompt_1 = '''
Here's the list of food items:
'''

story_gen_user_prompt_2 = '''
Please generate the story based on the food items and the theme. Your answer should not contain any other information or sentence but only strictly follow the output structure.
Also make sure that the story can also include the followings:
1) background about food, 
2) information about food ingredients, 
3) food smells and what it looks like while the story is interactive with the customers.
'''


image_generation_prompt_1 = '''

Here is an image generation task for you.
You'll be provided with a list of dishes and a dialogue imagined in a scenario where a customer made his/her order, and the cook & dishes unfold a story-like dialogue to him/her.
We want the image to be 2D from the top to look down at a table if the dishes. The cook does not have to be present. The image should describe the personality of each dish manifested in the dialogue. 

You need to generate only a single picture that best and must manifest:

(1) The dish itself realistically.
(2) The atmosphere and the sense that let the customer have a greater immersive sense into the dialogue.
(3) The personality / feature of the dish manifested in the dialogue.

Here is the theme description:
'''


image_generation_prompt_2 = '''
You are free to elaborate the meaning of the theme and the story based on the food items. Do not limit yourself to the key words and the examples above
Be Creative.


Here is the dialogue:
'''


image_generation_instr = '''
Now please generate the desired picture for the dish:

'''