from flask import Flask, request, url_for
from flask_cors import CORS
import pytesseract
from PIL import Image
import io
from llm import *

app = Flask(__name__)
CORS(app)

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'photo' in request.files:
        print('Photo uploaded')
        photo = request.files['photo']
        image = Image.open(io.BytesIO(photo.read()))
        text = pytesseract.image_to_string(image)
        
        res = ocr_text_to_dish(text)

        print(f"DIsh items: {res}")
        return res
    
    return {'error': 'No photo uploaded'}, 400

@app.route('/request', methods=['POST'])
def handle_request():
    # generate related images
    story, theme = item2story(request.json)
    img_urls = generate_image(request.json, story, theme)
    audio = generate_music(theme, story)

    res = {"audio": audio, "img": img_urls} # img contains list of image urls
    res.update(story)
    return res


if __name__ == '__main__':
    # model, tokenizer = model_init()
    app.run(debug=True)

    
