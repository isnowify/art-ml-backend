from openai import OpenAI
import ast
from theme_prompt import *
import random
import requests
import json
import time

openai_key = "sk-proj-fFleNGRFUdcceMMFWjSQT3BlbkFJIHNI8AdR9W2zbRImFL0w"
client = OpenAI(
    # This is the default and can be omitted
    api_key=openai_key,
)

def ocr_text_to_dish(text):

    print("Extracting dish names...")
    
    chat_completion = client.chat.completions.create(
        messages=[
            {
                "role": "system",
                "content": sys_prompt_itemname,
            },
            {
                "role": "user",
                "content": user_prompt_itemname + text,
            },
        ],
        model="gpt-4-turbo",
    )

    content = chat_completion.choices[0].message.content

    print("Dish names extracted successfully!")

    return ast.literal_eval(content)




def item2story(selected_items):
    # add the selected items between the use prompt 1 and 2, each item for one line
    user_prompt = story_gen_user_prompt_1
    for item in selected_items['selectedItems']:
        user_prompt += item + "\n"
    user_prompt += story_gen_sys_prompt_2

    #  randomly pick a theme and formmat the system prompt
    theme = random.choice(list(theme_prompt_dict.keys()))
    sys_prompt = story_gen_sys_prompt_1 + theme_prompt_dict[theme] + story_gen_sys_prompt_2

    # generate the story with gpt 4 toubo
    print(f"Generating story for theme {theme} ...")
    chat_completion = client.chat.completions.create(
        messages=[
            {
                "role": "system",
                "content": sys_prompt,
            },
            {
                "role": "user",
                "content": user_prompt,
            },
        ],
        model="gpt-4-turbo",
    )

    content = chat_completion.choices[0].message.content
    print("Story generated successfully!")

    return ast.literal_eval(content), theme


def generate_image(selected_items, story, theme):
    # add the story between the image generation prompt sys and user, each message for one line
    sys_prompt = image_generation_prompt_1 + theme + image_generation_prompt_2 + str(story)
    urls = []

    for item in selected_items['selectedItems']:
        # add the item name to the user prompt
        user_prompt = image_generation_instr + item
        request = sys_prompt + '\n' + user_prompt

        # generate the image with dalle3
        print(f"Length of prompt: {len(request)}")
        request = imgPromptRewrite(request)
        
        print(f"Generating image for {item}...")
        response = client.images.generate(
            model="dall-e-3",
            prompt=request,
            size="1024x1024",
            quality="standard",
            n=1,
        )

        img_url = response.data[0].url
        print(f"Image for {item}: {img_url}")
        urls.append(img_url)
    
    return urls


def imgPromptRewrite(prompt):

    instructions = '''
    You need to rewrite the prompt below to make it within 4000 characters.
    The output requirement is:
    (1) The prompt must be within 4000 characters.
    (2) The prompt must be clear and concise, with the same content as the original prompt.
    (3) The prompt must be perfectly suitable for DALL-E-3 to generate the image that the original prompt intended.
    (4) You must not include any other words besides the prompt you generate for further dalle3 image generation.
    '''

    user_prompt = prompt

    print("Generating new prompt...")

    new_prompt = client.chat.completions.create(
        messages=[
            {
                "role": "system",
                "content": instructions,
            },
            {
                "role": "user",
                "content": user_prompt,
            },
        ],
        model="gpt-4-turbo",
    )

    new_prompt = new_prompt.choices[0].message.content

    print(f"Rewritten prompt: {new_prompt}")

    return new_prompt


music_prompt_sys = '''
    You need to generate a prompt for suno AI to generate bakground music
    based on the story and theme description provided to you.


    The requirements for your output are:
    (1) The prompt must be within 10 words.
    (2) The prompt is descriptive sentences.

    An example of a prompt is:
    an emotional music about a bad breakup, sad, entangled.
'''

music_prompt_user_1 = '''
The theme desciption is:

'''

music_prompt_user_2 = '''
The story is:

'''

music_prompt_user_3 = '''
Please generate the prompt for the music generation as instructed.

'''

def musicPrompt(theme, story):

    user_prompt = music_prompt_user_1 + theme_prompt_dict[theme] + music_prompt_user_2 + str(story) + music_prompt_user_3

    print("Generating prompt for music generation...")

    new_prompt = client.chat.completions.create(
        messages=[
            {
                "role": "system",
                "content": music_prompt_sys,
            },
            {
                "role": "user",
                "content": user_prompt,
            },
        ],
        model="gpt-4-turbo",
    )

    new_prompt = new_prompt.choices[0].message.content

    print(f"Genmusic prompt: {new_prompt}")

    return new_prompt


def generate_music(theme, story):

    url = 'https://stimulate-thrower-seventh-dividend-thrif.vercel.app/api/generate'
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json'
    }

    prompt = musicPrompt(theme, story)

    data = {
        "prompt": prompt,
        "make_instrumental": True,
        "wait_audio": False
    }

    print(f"Generating music...\ndata: {json.dumps(data)}")
    response = requests.post(url, headers=headers, data=json.dumps(data))
    print(response.json())

    id1 = response.json()[0]['id']
    id2 = response.json()[1]['id']
    url1 = "https://stimulate-thrower-seventh-dividend-thrif.vercel.app/api/get?ids=" + id1
    url2 = "https://stimulate-thrower-seventh-dividend-thrif.vercel.app/api/get?ids=" + id2
    
    while True:
        response1 = requests.get(url1)
        response2 = requests.get(url2)
        if response1.json()[0]['audio_url']:
            print("Music generated successfully for url1!")
            return response1.json()[0]['audio_url']
        if response2.json()[0]['audio_url']:
            print("Music generated successfully for url2!")
            return response2.json()[0]['audio_url']
        
        
        # sleep for 5 seconds
        time.sleep(5)
    